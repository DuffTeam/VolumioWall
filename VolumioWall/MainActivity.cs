﻿using Android.App;
using Android.Widget;
using Android.OS;
using Volumio.RestSdk;
using Android.Runtime;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Android.Graphics;
using Android.Views;
using Volumio.RestSdk.Model;

namespace VolumioWall
{
    [Activity(Label = "VolumioWall", MainLauncher = true, Theme = "@android:style/Theme.Material.Light.NoActionBar")]
    [IntentFilter(new []{"android.intent.action.MAIN"}, Categories = new []
    {
        "android.intent.category.HOME",
        "android.intent.category.DEFAULT"
    })]
    public class MainActivity : Activity, GestureDetector.IOnGestureListener
    {
        private GestureDetector _detector;
        private VolumioClient _dev;
        private ImageView _albumArt;
        private TextView _titleView;
        private TextView _artistView;
        private TextView _albumView;
        private TextView _seekView;
        private ImageButton _playButton;
        private ImageButton _stopButton;
        private ImageButton _muteButton;
        private SeekBar _volumeSlider;
        private readonly HttpClient _http;
        private State _lastState;
        private Timer _updateTimer;

        private const string Server = "http://10.17.2.146";

        protected MainActivity(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public MainActivity()
        {
            _http = new HttpClient();
        }

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            
            //ActionBar.Hide();
            _albumArt = FindViewById<ImageView>(Resource.Id.albumArtView);
            _titleView = FindViewById<TextView>(Resource.Id.titleView);
            _artistView = FindViewById<TextView>(Resource.Id.artistView);
            _albumView = FindViewById<TextView>(Resource.Id.albumView);
            _seekView = FindViewById<TextView>(Resource.Id.seekView);

            _playButton = FindViewById<ImageButton>(Resource.Id.playButton);
            _stopButton = FindViewById<ImageButton>(Resource.Id.stopButton);
            _muteButton = FindViewById<ImageButton>(Resource.Id.muteButton);

            _volumeSlider = FindViewById<SeekBar>(Resource.Id.volumeSlider);

            _playButton.Click += OnPlay;
            _stopButton.Click += OnStop;
            _muteButton.Click += OnMute;

            _volumeSlider.ProgressChanged += OnVolumeChanged;

            _detector = new GestureDetector(this, this);

            _dev = new VolumioClient(Server);
            _updateTimer = new Timer(OnTimerTick, null, Timeout.Infinite, Timeout.Infinite);
            await UpdateUIAsync();
        }

        private void OnTimerTick(object state)
        {
            RunOnUiThread(async () => await UpdateUIAsync());
        }

        private async Task UpdateUIAsync()
        {
            try
            {
                var state = await _dev.GetStateAsync();
                await SetAlbumArtAsync(state.AlbumArt);
                _titleView.Text = state.Title;
                _artistView.Text = state.Artist;
                _albumView.Text = state.Album;
                _volumeSlider.Progress = state.Volume ?? 0;
                _lastState = state;

                if (state.Seek.HasValue)
                {
                    var ts = TimeSpan.FromMilliseconds(state.Seek.Value);
                    _seekView.Text = ts.ToString("%m\\:ss");
                }
                else
                    _seekView.Text = "";

                var playResource = state.Status == VolumioStatus.Play
                    ? Resource.Drawable.pause
                    : Resource.Drawable.play;
                _playButton.SetImageResource(playResource);

                var muteResource = state.Mute ?? false
                    ? Resource.Drawable.unmute
                    : Resource.Drawable.mute;
                _muteButton.SetImageResource(muteResource);
            }
            finally
            {
                _updateTimer.Change(1000, Timeout.Infinite);
            }
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            return _detector.OnTouchEvent(e) || base.OnTouchEvent(e);
        }

        private async void OnVolumeChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            var progress = e.Progress;
            await _dev.SetVolumeAsync(progress);
        }

        private int _premuteVolume;
        private async void OnMute(object sender, EventArgs args)
        {
            var isMuted = _lastState.Mute ?? false;

            if (isMuted)
            {
                await _dev.UnmuteAsync();
                await _dev.SetVolumeAsync(_premuteVolume);
            }
            else
            {
                _premuteVolume = _lastState.Volume ?? 0;
                await _dev.MuteAsync();
            }

            await Task.Delay(500);
            await UpdateUIAsync();
        }

        private async void OnStop(object sender, EventArgs args)
        {
            await _dev.StopAsync();
            //await Task.Delay(500);
            await UpdateUIAsync();
        }

        private async void OnPlay(object sender, EventArgs args)
        {
            if (_lastState.Status == VolumioStatus.Play)
                await _dev.PauseAsync();
            else
                await _dev.PlayAsync();

            //await Task.Delay(500);
            await UpdateUIAsync();
        }

        private string _lastAlbumArtUri;
        private async Task SetAlbumArtAsync(string uri)
        {
            if (uri == _lastAlbumArtUri)
                return;
            _lastAlbumArtUri = uri;
            if (uri.StartsWith("/"))
                uri = $"{Server}{uri}";

            using (var stream = await _http.GetStreamAsync(uri))
            {
                var bitmap = await BitmapFactory.DecodeStreamAsync(stream);
                _albumArt.SetImageBitmap(bitmap);
            }
        }

        public bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            var dx = e2.RawX - e1.RawX;
            var dy = e2.RawY - e1.RawY;
            var len = Math.Sqrt(dx * dx + dy * dy);
            if (len < 150)
                return false;

            var angle = Math.Atan2(e2.RawY - e1.RawY, e2.RawX - e1.RawX) * (180.0 / Math.PI);
            if (angle < 0)
                angle += 360;

            if (angle >= 315)
                OnFlingRight();
            else if (angle >= 225)
                OnFlingUp();
            else if (angle >= 135)
                OnFlingLeft();
            else if (angle >= 45)
                OnFlingDown();
            else
                OnFlingRight();

            return true;
        }

        private void OnFlingUp()
        {
        }

        private void OnFlingDown()
        {
        }

        private async void OnFlingLeft()
        {
            await _dev.NextAsync();
            //await Task.Delay(500);
            await UpdateUIAsync();
        }

        private async void OnFlingRight()
        {
            await _dev.PreviousAsync();
            //await Task.Delay(500);
            await UpdateUIAsync();
        }

        public bool OnDown(MotionEvent e) => false;
        public void OnLongPress(MotionEvent e) { }
        public bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) => false;
        public void OnShowPress(MotionEvent e) { }
        public bool OnSingleTapUp(MotionEvent e) => false;
    }
}
